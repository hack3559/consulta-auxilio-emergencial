<?php

Class Caixa{

	public function Clear(){
		if (file_exists(getcwd() . ('/cookie.txt'))) {
			@unlink("cookie.txt");
		}
	}

	public function Request($url,$postdata,$headers=array(),$use_post,$posttype){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if($use_post){
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $posttype);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
		}
		curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_POSTREDIR, 3);
		curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		// curl_setopt($ch, CURLOPT_PROXY, "185.30.232.18:80");
		// curl_setopt($ch, CURLOPT_PROXYUSERPWD, "qvhkmgok-rotate:lrtpdyfocpsb");
		curl_setopt($ch, CURLOPT_COOKIE, getcwd().'/cookie.txt');
		curl_setopt($ch, CURLOPT_COOKIEJAR, getcwd().'/cookie.txt');
		curl_setopt($ch, CURLOPT_COOKIEFILE, getcwd().'/cookie.txt');
		curl_setopt($ch, CURLOPT_LOW_SPEED_LIMIT, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$exec = curl_exec($ch) or die(curl_error($ch));
		curl_close($ch);
		return $exec;
	}
}
$chk = new Caixa();

# Mensagens
# {"codigo":417,"mensagem":"CPF na base do CADUN"}

$nome = $_REQUEST['nome'];
$cpf = $_REQUEST['cpf'];
$nascimento = explode('/', $_REQUEST['nascimento']);
$dia = $nascimento[0];$mes = $nascimento[1];$ano = $nascimento[2];
$mae = $_REQUEST['mae'];

$postdata = '{"nome":"'.$nome.'","cpf":'.$cpf.',"dataNascimento":"'.$ano.'-'.$mes.'-'.$dia.'","nomeMae":"'.$mae.'"}';
$r = $chk->Request(
	"https://auxilio.caixa.gov.br/api/cadastro",
	$postdata,
	array(
		'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0',
		'Accept: application/json, text/plain, */*',
		'Accept-Language: pt-BR,pt;q=0.8,en-US;q=0.5,en;q=0.3',
		'Content-Type: application/json',
		'Request-Id: |fa689ecc39ca40019e8a1efbe2ff3d19.6082ad49d4c1441d',
		'Request-Context: appId=cid-v1:d98a79eb-22ee-41b8-8a38-969cf6bcf67d',
		'Content-Length: '.strlen($postdata),
		'Origin: https://auxilio.caixa.gov.br',
		'Connection: keep-alive',
		'Referer: https://auxilio.caixa.gov.br/',
		'Cookie: _ga=GA1.3.1500378162.1586211123; _gid=GA1.3.1594692339.1586211123; _fbp=fb.2.1586211124071.1929446504; ai_user=EDGNk|2020-04-07T14:26:32.694Z; ai_session=WDC9d|1586269595219|1586270122091',
		'TE: Trailers',
		'Pragma: no-cache',
		'Cache-Control: no-cache'
	),
	true,
	"POST"
);

print $r;