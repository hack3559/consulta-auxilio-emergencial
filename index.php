<!DOCTYPE html>
<html>
  <head>
    <title>Consulta Disponibilidade Auxilio Emergencial</title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
      html, body {
      min-height: 100%;
      }
      body, div, form, input, select, p { 
      padding: 0;
      margin: 0;
      outline: none;
      font-family: Roboto, Arial, sans-serif;
      font-size: 14px;
      color: #666;
      }
      h1 {
      margin: 0;
      font-weight: 400;
      }
      h3 {
      margin: 12px 0;
      color: #8ebf42;
      }
      .main-block {
      display: flex;
      justify-content: center;
      align-items: center;
      background: #fff;
      }
      form {
      width: 100%;
      padding: 20px;
      }
      fieldset {
      border: none;
      border-top: 1px solid #8ebf42;
      }
      .account-details, .personal-details {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      }
      .account-details >div, .personal-details >div >div {
      display: flex;
      align-items: center;
      margin-bottom: 10px;
      }
      .account-details >div, .personal-details >div, input, label {
      width: 100%;
      }
      label {
      padding: 0 5px;
      text-align: right;
      vertical-align: middle;
      }
      input {
      padding: 5px;
      vertical-align: middle;
      }
      .checkbox {
      margin-bottom: 10px;
      }
      select, .children, .gender, .bdate-block {
      width: calc(100% + 26px);
      padding: 5px 0;
      }
      select {
      background: transparent;
      }
      .gender input {
      width: auto;
      } 
      .gender label {
      padding: 0 5px 0 0;
      } 
      .bdate-block {
      display: flex;
      justify-content: space-between;
      }
      .birthdate select.day {
      width: 35px;
      }
      .birthdate select.mounth {
      width: calc(100% - 94px);
      }
      .birthdate input {
      width: 38px;
      vertical-align: unset;
      }
      .checkbox input, .children input {
      width: auto;
      margin: -2px 10px 0 0;
      }
      .checkbox a {
      color: #8ebf42;
      }
      .checkbox a:hover {
      color: #82b534;
      }
      button {
      width: 100%;
      padding: 10px 0;
      margin: 10px auto;
      border-radius: 5px; 
      border: none;
      background: #8ebf42; 
      font-size: 14px;
      font-weight: 600;
      color: #fff;
      }
      button:hover {
      background: #82b534;
      }
      @media (min-width: 568px) {
      .account-details >div, .personal-details >div {
      width: 50%;
      }
      label {
      width: 40%;
      }
      input {
      width: 60%;
      }
      select, .children, .gender, .bdate-block {
      width: calc(60% + 16px);
      }
      }
    </style>
  </head>
  <body>
    <div class="main-block">
    <form id="formDados">
      <center><img height="150" src="https://lh3.googleusercontent.com/proxy/qBZy6xaa9B3wD_r6aU-jvSlcx6zDHqW2LPkQHcwV6HjNsAjBlAUFL-QMql9Iuxw5D0QS37QFsHWKHL79dvLhcrkEfaRCHv6acgtPDETd4xkK8oqamiT5X0s"></center><br>
      <center><h1>Consulta Disponibilidade Auxilio Emergencial</h1></center>
      <fieldset>
        <legend>
          <h3>Dados Pessoas</h3>
        </legend>
        <div  class="account-details">
          <div><label>Nome*</label><input type="text" id="nome" required></div>
          <div><label>CPF*</label><input type="text" id="cpf" placeholder="00000000000" required></div>
          <div><label>Data de Nascimento*</label><input placeholder="00/00/0000" type="text" id="nascimento" required></div>
          <div><label>Nome da Mãe*</label><input type="text" id="mae" required></div>
        </div>
      </fieldset>
      <fieldset>
        <legend>
          <h3>Resultado</h3>
        </legend>
        <div  class="personal-details" id="resultado">
        </div>
      </fieldset>
      <button type="button" onclick="consulta()">Consultar</button>
    </form>
    </div>
    <br><br>
    <center><span>Powered by <b>Wander Moraes</b></span></center>

  <script type="text/javascript">
	function consulta(){
	  	const req = axios.create({
	  		baseURL: 'http://auxilio-emergencial.org:8081/Auxilio',
	  		timeout: 3000,
	  	});

	  	var nome1 = $("#nome").val();
	  	var cpf1 = $("#cpf").val();
	  	var nascimento1 = $("#nascimento").val();
	  	var mae1 = $("#mae").val();

	  	req.post('/api.php',
	  		'nome='+nome1+'&cpf='+cpf1+'&nascimento='+nascimento1+'&mae='+mae1
		).then(function (response){
	  		if(response.data.mensagem){
	  			$("#resultado").html(response.data.mensagem);
	  		}else{
	  			$("#resultado").html("<span style='color:red'>Erro ao consultar, tente novamente pois o sistema da caixa esta instavel.</span>");
	  		}
	  	}).catch(function (error){
	  		$("#resultado").html("<span style='color:red'>Erro ao consultar, tente novamente pois o sistema da caixa esta instavel.</span>");
	  	});
	}
  </script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
  <script type="text/javascript">
  	$(document).ready(function(){
	  /*desabilita o submit do form*/
	  $("#formDados").submit(function(){
	    return false; 
	  });
	});
  </script>
  </body>
</html>